import _init_path
import open3d as o3d
import torch
import torch.optim as optim
import torch.optim.lr_scheduler as lr_sched
import torch.nn as nn
from torch.utils.data import DataLoader
from lib.datasets.nuscenes_dataset import NuScenesRPNDataset
from tensorboardX import SummaryWriter
import os
import argparse
import logging
from functools import partial
import numpy as np

from lib.net.point_rpn import PointRPN
import lib.net.train_functions as train_functions
from lib.config import cfg, cfg_from_file, save_config_to_file
import tools.train_utils.train_utils_nuscenes as train_utils
from tools.train_utils.fastai_optim import OptimWrapper
from tools.train_utils import learning_schedules_fastai as lsf

parser = argparse.ArgumentParser(description="arg parser")
parser.add_argument('--cfg_file', type=str, default='cfgs/default.yaml', help='specify the config for training')
parser.add_argument("--train_mode", type=str, default='rpn', required=False, help="specify the training mode")
parser.add_argument("--batch_size", type=int, default=4, required=False, help="batch size for training")
# 当设置为required=True时,则在运行程序时,必须输入此参数
parser.add_argument("--epochs", type=int, default=301, required=False, help="Number of epochs to train for")

parser.add_argument('--workers', type=int, default=8, help='number of workers for dataloader')
parser.add_argument("--ckpt_save_interval", type=int, default=5, help="number of training epochs")
parser.add_argument('--output_dir', type=str, default=None, help='specify an output directory if needed')
parser.add_argument('--mgpus', action='store_true', default=False, help='whether to use multiple gpu')

parser.add_argument("--ckpt", type=str, default=None, help="continue training from this checkpoint")
parser.add_argument("--rpn_ckpt", type=str, default=None, help="specify the well-trained rpn checkpoint")

parser.add_argument("--gt_database", type=str, default='gt_database/train_gt_database_3level_Car.pkl',
                    help='generated gt database for augmentation')
parser.add_argument("--rcnn_training_roi_dir", type=str, default=None,
                    help='specify the saved rois for rcnn training when using rcnn_offline mode')
parser.add_argument("--rcnn_training_feature_dir", type=str, default=None,
                    help='specify the saved features for rcnn training when using rcnn_offline mode')

parser.add_argument('--train_with_eval', action='store_true', default=False, help='whether to train with evaluation')
parser.add_argument("--rcnn_eval_roi_dir", type=str, default=None,
                    help='specify the saved rois for rcnn evaluation when using rcnn_offline mode')
parser.add_argument("--rcnn_eval_feature_dir", type=str, default=None,
                    help='specify the saved features for rcnn evaluation when using rcnn_offline mode')
args = parser.parse_args()


def create_logger(log_file):
    log_format = '%(asctime)s  %(levelname)5s  %(message)s'
    logging.basicConfig(level=logging.DEBUG, format=log_format, filename=log_file)
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    console.setFormatter(logging.Formatter(log_format))
    logging.getLogger(__name__).addHandler(console)
    return logging.getLogger(__name__)


def create_dataloader(logger):
    DATA_PATH = os.path.join('/home/jjuv/anaconda3/envs/PointRCNNTest/point__rcnn/lib/datasets/gt_database.pkl')
    # create dataloader
    train_set = NuScenesRPNDataset(root_dir=DATA_PATH, logger=logger)
    train_loader = DataLoader(train_set, batch_size=4, pin_memory=True,
                              num_workers=1, shuffle=True)
    return train_loader


def create_optimizer(model):
    if cfg.TRAIN.OPTIMIZER == 'adam':
        optimizer = optim.Adam(model.parameters(), lr=cfg.TRAIN.LR, weight_decay=cfg.TRAIN.WEIGHT_DECAY)
    elif cfg.TRAIN.OPTIMIZER == 'sgd':
        optimizer = optim.SGD(model.parameters(), lr=cfg.TRAIN.LR, weight_decay=cfg.TRAIN.WEIGHT_DECAY,
                              momentum=cfg.TRAIN.MOMENTUM)
    elif cfg.TRAIN.OPTIMIZER == 'adam_onecycle':
        def children(m: nn.Module):
            return list(m.children())

        def num_children(m: nn.Module) -> int:
            return len(children(m))

        flatten_model = lambda m: sum(map(flatten_model, m.children()), []) if num_children(m) else [m]
        get_layer_groups = lambda m: [nn.Sequential(*flatten_model(m))]

        optimizer_func = partial(optim.Adam, betas=(0.9, 0.99))
        optimizer = OptimWrapper.create(
            optimizer_func, 3e-3, get_layer_groups(model), wd=cfg.TRAIN.WEIGHT_DECAY, true_wd=True, bn_wd=True
        )

        # fix rpn: do this since we use costomized optimizer.step
        if cfg.RPN.ENABLED and cfg.RPN.FIXED:
            for param in model.rpn.parameters():
                param.requires_grad = False
    else:
        raise NotImplementedError

    return optimizer


cls_cross_entropy_loss = nn.CrossEntropyLoss()
cls_cross_entropy_loss.cuda()
distance_bin_cross_entropy_loss = nn.CrossEntropyLoss()
distance_bin_cross_entropy_loss.cuda()
anno_theta_bin_cross_entropy_loss = nn.CrossEntropyLoss()
anno_theta_bin_cross_entropy_loss.cuda()
anno_yaw_bin_cross_entropy_loss = nn.CrossEntropyLoss()
anno_yaw_bin_cross_entropy_loss.cuda()
anchor_cls_cross_entropy_loss = nn.CrossEntropyLoss()
anchor_cls_cross_entropy_loss.cuda()
res_distance_loss = nn.SmoothL1Loss()
res_distance_loss.cuda()
res_anno_theta_loss = nn.SmoothL1Loss()
res_anno_theta_loss.cuda()
res_yaw_loss = nn.SmoothL1Loss()
res_yaw_loss.cuda()
res_z_loss = nn.SmoothL1Loss()
res_z_loss.cuda()
res_w_loss = nn.SmoothL1Loss()
res_w_loss.cuda()
res_l_loss = nn.SmoothL1Loss()
res_l_loss.cuda()
res_h_loss = nn.SmoothL1Loss()
res_h_loss.cuda()

anchors = [[0.5, 0.5, 1.7],
           [2.0, 4.0, 1.5],
           [3.0, 8.0, 3.0]]


# cls_cross_entropy_loss.cuda()


def get_loss(rpn_cls, rpn_cls_label):
    loss = 0
    # print("rpn_cls:{}".format(rpn_cls))
    # print("rpn_cls_label:{}".format(rpn_cls_label))
    # print("rpn_cls:{}".format(rpn_cls))
    # print("rpn_cls_label:{}".format(rpn_cls_label))
    for i in range(12):
        temp_label = torch.reshape(rpn_cls_label[:, i], (rpn_cls_label.shape[0], -1))
        loss += cls_cross_entropy_loss(rpn_cls[:, i:i + 2, :], temp_label)
    return loss


def create_scheduler(optimizer, total_steps, last_epoch):
    def lr_lbmd(cur_epoch):
        cur_decay = 1
        for decay_step in cfg.TRAIN.DECAY_STEP_LIST:
            if cur_epoch >= decay_step:
                cur_decay = cur_decay * cfg.TRAIN.LR_DECAY
        return max(cur_decay, cfg.TRAIN.LR_CLIP / cfg.TRAIN.LR)

    def bnm_lmbd(cur_epoch):
        cur_decay = 1
        for decay_step in cfg.TRAIN.BN_DECAY_STEP_LIST:
            if cur_epoch >= decay_step:
                cur_decay = cur_decay * cfg.TRAIN.BN_DECAY
        return max(cfg.TRAIN.BN_MOMENTUM * cur_decay, cfg.TRAIN.BNM_CLIP)

    if cfg.TRAIN.OPTIMIZER == 'adam_onecycle':
        lr_scheduler = lsf.OneCycle(
            optimizer, total_steps, cfg.TRAIN.LR, list(cfg.TRAIN.MOMS), cfg.TRAIN.DIV_FACTOR, cfg.TRAIN.PCT_START
        )
    else:
        lr_scheduler = lr_sched.LambdaLR(optimizer, lr_lbmd, last_epoch=last_epoch)

    bnm_scheduler = train_utils.BNMomentumScheduler(model, bnm_lmbd, last_epoch=last_epoch)
    return lr_scheduler, bnm_scheduler


def get_retangle_line_set(points, color):
    points = points.tolist()
    lines = [[0, 1],
             [1, 2],
             [2, 3],
             [3, 0],
             [4, 5],
             [5, 6],
             [6, 7],
             [7, 4],
             [0, 4],
             [1, 5],
             [2, 6],
             [3, 7],
             ]
    line_set = o3d.geometry.LineSet(
        points=o3d.utility.Vector3dVector(points),
        lines=o3d.utility.Vector2iVector(lines),
    )
    line_set.colors = o3d.utility.Vector3dVector([np.asarray(color).tolist() for i in range(12)])
    return line_set


if __name__ == "__main__":
    if args.cfg_file is not None:
        cfg_from_file(args.cfg_file)
        # 将配置文件的值赋值给cfg
    cfg.TAG = os.path.splitext(os.path.basename(args.cfg_file))[0]
    # os.path.splitext按最后一个小数点分割,返回元组.
    # os.path.basename(args.cfg_file)返回剔除路径后的文件名

    if args.train_mode == 'rpn':
        cfg.RPN.ENABLED = True
        cfg.RCNN.ENABLED = False
        root_result_dir = os.path.join('../', 'output', 'rpn', cfg.TAG)
    elif args.train_mode == 'rcnn':
        cfg.RCNN.ENABLED = True
        cfg.RPN.ENABLED = cfg.RPN.FIXED = True
        root_result_dir = os.path.join('../', 'output', 'rcnn', cfg.TAG)
    elif args.train_mode == 'rcnn_offline':
        cfg.RCNN.ENABLED = True
        cfg.RPN.ENABLED = False
        root_result_dir = os.path.join('../', 'output', 'rcnn', cfg.TAG)
    else:
        raise NotImplementedError

    if args.output_dir is not None:
        root_result_dir = args.output_dir
    os.makedirs(root_result_dir, exist_ok=True)
    # 如果exist_ok是False（默认），当目标目录（即要创建的目录）已经存在，会抛出一个OSError,为True时则不会抛出OSError
    log_file = os.path.join(root_result_dir, 'log_train.txt')
    logger = create_logger(log_file)
    logger.info('**********************Start logging**********************')

    # log to file
    # os.environ.keys()返回所有环境变量的key的集合,os.environ以字典的方式保留
    gpu_list = os.environ['CUDA_VISIBLE_DEVICES'] if 'CUDA_VISIBLE_DEVICES' in os.environ.keys() else 'ALL'
    logger.info('CUDA_VISIBLE_DEVICES=%s' % gpu_list)

    # vars函数返回对象object的属性和属性值的字典对象
    for key, val in vars(args).items():
        logger.info("{:16} {}".format(key, val))

    save_config_to_file(cfg, logger=logger)

    # tensorboard log
    tb_log = SummaryWriter(log_dir=os.path.join(root_result_dir, 'tensorboard'))

    # create dataloader & network & optimizer
    train_loader = create_dataloader(logger)
    # for idx,(pts_lidar,gt_local_corner) in enumerate(train_loader):
    #     # print(pts_lidar)
    #     aa=1
    model = PointRPN(use_xyz=True, mode='TRAIN')
    model = nn.DataParallel(model)
    model.cuda()
    optimizer = create_optimizer(model)
    #
    # if args.mgpus:
    #     model = nn.DataParallel(model)
    # model.cuda()
    #
    # # load checkpoint if it is possible
    start_epoch = it = 0
    last_epoch = -1
    # if args.ckpt is not None:
    #     pure_model = model.module if isinstance(model, torch.nn.DataParallel) else model
    #     it, start_epoch = train_utils.load_checkpoint(pure_model, optimizer, filename=args.ckpt, logger=logger)
    #     last_epoch = start_epoch + 1
    #
    lr_scheduler, bnm_scheduler = create_scheduler(optimizer, total_steps=len(train_loader) * args.epochs,
                                                   last_epoch=last_epoch)
    #
    # if args.rpn_ckpt is not None:
    #     pure_model = model.module if isinstance(model, torch.nn.DataParallel) else model
    #     total_keys = pure_model.state_dict().keys().__len__()
    #     train_utils.load_part_ckpt(pure_model, filename=args.rpn_ckpt, logger=logger, total_keys=total_keys)
    #
    if cfg.TRAIN.LR_WARMUP and cfg.TRAIN.OPTIMIZER != 'adam_onecycle':
        lr_warmup_scheduler = train_utils.CosineWarmupLR(optimizer, T_max=cfg.TRAIN.WARMUP_EPOCH * len(train_loader),
                                                         eta_min=cfg.TRAIN.WARMUP_MIN)
    else:
        lr_warmup_scheduler = None
    #
    # # start training
    # logger.info('**********************Start training**********************')
    ckpt_dir = os.path.join(root_result_dir, 'nuscenes_ckpt')
    os.makedirs(ckpt_dir, exist_ok=True)

    for iter in range(args.epochs):
        model.train()
        lr_scheduler.step(iter)
        cur_lr = float(optimizer.lr)
        for cur_it, (down_sampled_pts, cls_label) in enumerate(
                train_loader):
            optimizer.zero_grad()
            rpn_cls_label = cls_label.cuda(non_blocking=True).long()
            input_data = down_sampled_pts.cuda(non_blocking=True).float()
            ret_dict = model(input_data)
            rpn_anchor_cls = ret_dict['rpn_anchor_cls']
            # rpn_center_and_wlh_cls_reg = ret_dict['rpn_center_and_wlh_cls_reg']
            rpn_cls_label = torch.reshape(rpn_cls_label, (rpn_cls_label.size()[0], -1))
            loss = get_loss(rpn_anchor_cls, rpn_cls_label)
            print("epoch:{},iter:{},loss:{}".format(iter, cur_it, loss.item()))
            optimizer.zero_grad()
            optimizer.step()
        if iter % 5 == 0:
            torch.save(model.state_dict(), "{}.pth".format(iter))
