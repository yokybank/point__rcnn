import _init_path
import open3d as o3d
import torch
import torch.optim as optim
import torch.optim.lr_scheduler as lr_sched
import torch.nn as nn
from torch.utils.data import DataLoader
from lib.datasets.nuscenes_dataset import NuScenesRPNDataset
from tensorboardX import SummaryWriter
import os
import argparse
import logging
from functools import partial
from lib.datasets.nuscenes_inference_dataset import NuScenesInferenceDataset
from lib.net.point_rpn import PointRPN
import numpy as np

hunman_class_list = [
    "human.pedestrian.adult",
    "human.pedestrian.child",
    "human.pedestrian.construction_worker",
    "human.pedestrian.personal_mobility",
    "human.pedestrian.police_officer",
    "human.pedestrian.stroller",
    "human.pedestrian.wheelchair"]

vehicle_class_list = [
    "vehicle.bicycle",
    "vehicle.bus.bendy",
    "vehicle.bus.rigid",
    "vehicle.car",
    "vehicle.construction",
    "vehicle.emergency.ambulance",
    "vehicle.emergency.police",
    "vehicle.motorcycle",
    "vehicle.trailer",
    "vehicle.truck"]


def get_retangle_line_set(points, color):
    points = points.tolist()
    lines = [[0, 1],
             [1, 2],
             [2, 3],
             [3, 0],
             [4, 5],
             [5, 6],
             [6, 7],
             [7, 4],
             [0, 4],
             [1, 5],
             [2, 6],
             [3, 7],
             ]
    line_set = o3d.geometry.LineSet(
        points=o3d.utility.Vector3dVector(points),
        lines=o3d.utility.Vector2iVector(lines),
    )
    line_set.colors = o3d.utility.Vector3dVector([np.asarray(color).tolist() for i in range(12)])
    return line_set


def create_dataloader():
    DATA_PATH = os.path.join(
        '/home/jjuv/anaconda3/envs/PointRCNNTest/point__rcnn/lib/datasets/sample_data_json_path.pkl')
    # create dataloader
    test_dataset = NuScenesInferenceDataset(root_dir=DATA_PATH)
    test_loader = DataLoader(test_dataset, batch_size=1, pin_memory=True,
                             num_workers=0, shuffle=False)
    return test_loader


anchors = [[0.5, 1.0, 1.7, 0.0],
           [0.5, 1.0, 1.7, 45.0],
           [0.5, 1.0, 1.7, 90.0],
           [0.5, 1.0, 1.7, 135.0],
           [3.0, 5.0, 1.7, 0.0],
           [3.0, 5.0, 1.7, 45.0],
           [3.0, 5.0, 1.7, 90.0],
           [3.0, 5.0, 1.7, 135.0],
           [4.0, 10.0, 2.5, 0.0],
           [4.0, 10.0, 2.5, 45.0],
           [4.0, 10.0, 2.5, 90.0],
           [4.0, 10.0, 2.5, 135.0]]


def get_corners_acrroding_to_wlh_theta(wlh_theta):
    # wlh_theta[3]角度坐标系同nuscenes的角度坐标系,y轴负轴为0度,逆时针为正,顺时针为负,[-pi,pi]
    eight_corners = np.asarray(
        [[-wlh_theta[0] / 2, wlh_theta[0] / 2, wlh_theta[0] / 2, -wlh_theta[0] / 2, -wlh_theta[0] / 2,
          wlh_theta[0] / 2, wlh_theta[0] / 2, -wlh_theta[0] / 2],
         [wlh_theta[1] / 2, wlh_theta[1] / 2, wlh_theta[1] / 2, wlh_theta[1] / 2, -wlh_theta[1] / 2,
          -wlh_theta[1] / 2, -wlh_theta[1] / 2, -wlh_theta[1] / 2],
         [wlh_theta[2] / 2, wlh_theta[2] / 2, -wlh_theta[2] / 2, -wlh_theta[2] / 2, wlh_theta[2] / 2,
          wlh_theta[2] / 2, -wlh_theta[2] / 2, -wlh_theta[2] / 2]])
    rotate_maxtrix = np.asarray([[np.cos(wlh_theta[3]), -np.sin(wlh_theta[3]), 0],
                                 [np.sin(wlh_theta[3]), np.cos(wlh_theta[3]), 0],
                                 [0, 0, 1]])
    eight_corners = np.dot(rotate_maxtrix, eight_corners)
    return eight_corners


if __name__ == "__main__":

    # create dataloader & network & optimizer
    test_loader = create_dataloader()
    rpn_net = PointRPN(use_xyz=True, mode="TRAIN")
    rpn_net = nn.DataParallel(rpn_net)
    rpn_net.load_state_dict(torch.load("240.pth"))
    rpn_net.cuda()
    soft_max_func = nn.Softmax(dim=1)

    for iter in range(200):
        rpn_net.eval()
        for cur_it, (pts_cloud, obj_list, obj_corresponding_around_pts, annotations) in enumerate(test_loader):
            obj_corresponding_prediction = []
            predicted_boxes = []
            # print("obj_corresponding_around_pts.size:{}".format(len(obj_corresponding_around_pts)))
            for count in range(len(obj_corresponding_around_pts)):
                input_data = obj_corresponding_around_pts[count].cuda(non_blocking=True).float()
                ret_dict = rpn_net.forward(input_data)
                rpn_cls = ret_dict['rpn_anchor_cls']
                # print("rpn_cls:{}".format(rpn_cls))
                # print("rpn_cls.data:{}".format(rpn_cls.data))
                predicted_anchor_list = []
                predicted_anchor_probility = []
                for len_count in range(12):
                    temp_feature = rpn_cls.data[:, len_count * 2:len_count * 2 + 2, :]
                    if temp_feature[0, 0, 0] > 0.0001 or temp_feature[0, 1, 0] > 0.0001:
                        _, predicted_anchor = torch.max(temp_feature, dim=1)
                        probility = soft_max_func(temp_feature)
                        # nn.module.sof
                        # print("temp_feature:{}".format(temp_feature))
                        # print("probility:{}".format(probility))
                        # print()
                        if predicted_anchor.cpu() == 1:
                            if probility[0, 1, 0] > 0.1:
                                predicted_anchor_list.append(len_count)
                                predicted_anchor_probility.append(probility[0, 1, 0])
                                # print("probility[0, 0, 0]:{}".format(probility[0, 1, 0]))

                max_count_index = 0
                max_prob = 0.0
                for len_count in range(len(predicted_anchor_probility)):
                    if max_prob < predicted_anchor_probility[len_count]:
                        max_prob = predicted_anchor_probility[len_count]
                        max_count_index = predicted_anchor_list[len_count]
                if max_prob > 0.05:
                    print(max_prob)
                    # for len_count in range(len(predicted_anchor_list)):

                    eight_corners = get_corners_acrroding_to_wlh_theta(anchors[max_count_index])
                    for corner_count in range(8):
                        eight_corners[0, corner_count] = eight_corners[0, corner_count] + obj_list[count]['center'][
                            0, 0]
                        eight_corners[1, corner_count] = eight_corners[1, corner_count] + obj_list[count]['center'][
                            0, 1]
                        eight_corners[2, corner_count] = eight_corners[2, corner_count] + obj_list[count]['center'][
                            0, 2]
                    predicted_boxes.append(eight_corners)
                    # print("obj_list[count]['center']:{}".format(obj_list[count]['center']))
                    # print("eight_corners:{}".format(eight_corners))
                # print("predicted_anchor_list:{}".format(predicted_anchor_list))
                # print()
                # print(rpn_center_and_wlh_cls_reg.data)
                # _, pred = torch.max(rpn_cls.data, dim=1)
                # obj_corresponding_prediction.append(pred.cpu())
                # if pred.cpu() != 0:

                # print(rpn_center_and_wlh_cls_reg)
                # print("distance:{}".format(distance))
                # print("distance_center_theta:{}".format(distance_center_theta))
                # print("yaw_theta:{}".format(yaw_theta))
                # print("w:{}".format(w))
                # print("l:{}".format(l))
                # print("h:{}".format(h))

            # print("pred:{}".format(pred.cpu()))
            data_to_draw = []
            # for box_count in range(len(predicted_boxes)):
            #     data_to_draw.extend([get_retangle_line_set(predicted_boxes[box_count].T,
            #                                                [0, 0, 0])])
            #
            for count in range(len(annotations)):
                # print(annotations[count])
                if annotations[count]['category_name'][0] in hunman_class_list:
                    data_to_draw.extend([get_retangle_line_set(annotations[count]['corners'][0, :, :].numpy().T,
                                                               [1, 0, 0])])
                if annotations[count]['category_name'][0] in vehicle_class_list:
                    # print('corners:{}'.format(annotations[count]['corners']))
                    data_to_draw.extend([get_retangle_line_set(annotations[count]['corners'][0, :, :].numpy().T,
                                                               [0, 0, 1])])
            for count in range(len(predicted_boxes)):
                data_to_draw.extend([get_retangle_line_set(np.asarray(predicted_boxes[count]).T,
                                                           [0, 0, 0])])

            # #
            # for count in range(len(obj_corresponding_prediction)):
            #     pcd = o3d.geometry.PointCloud()
            #     # print("obj_corresponding_prediction[count]:{}".format(obj_corresponding_prediction[count]))
            #     if obj_corresponding_prediction[count] == 1:
            #         pcd.points = o3d.utility.Vector3dVector(np.asarray(obj_list[count]['center']))
            #         pcd.paint_uniform_color([1, 0, 0])
            #         data_to_draw.extend([pcd])
            #     if obj_corresponding_prediction[count] == 2:
            #         pcd.points = o3d.utility.Vector3dVector(np.asarray(obj_list[count]['center']))
            #         pcd.paint_uniform_color([0, 0, 1])
            #         data_to_draw.extend([pcd])

            FOR1 = o3d.geometry.TriangleMesh.create_coordinate_frame(size=3, origin=[0, 0, 0])
            pcd = o3d.geometry.PointCloud()
            pcd.points = o3d.utility.Vector3dVector((np.asarray(pts_cloud)[0:3, :]).T)
            pcd.paint_uniform_color([0.3, 0.8, 0.3])
            data_to_draw.extend([FOR1, pcd])
            o3d.visualization.draw_geometries(data_to_draw)
