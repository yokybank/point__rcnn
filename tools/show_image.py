import cv2
import numpy as np
import  time
# while True:
count = 0.0
data = []
corners = []
pred_clas_selected = []
objs = []
for line in open("./image_detection_result.txt"):
    line = line.strip()
    temp_line = line.split(',')
    if len(temp_line) == 2:
        print(temp_line[0],temp_line[1])
        temp_corner = [round(float(temp_line[0])), round(float(temp_line[1]))]
        corners.append(temp_corner)
    elif len(temp_line) == 1:
        temp_corner = [int(temp_line[0])]
        pred_clas_selected.append(temp_corner[0])
        objs.append(corners.copy())
        corners.clear()
objs = np.asarray(objs)
print("objs:{}".format(objs))
pred_clas_selected = np.asarray(pred_clas_selected)
thickness = 2
line_type = 2
with open("./image_path.txt", "r") as f:  # 打开文件
    data = f.read()  # 读取文件
    f.close()
    image_data = cv2.imread(data)

for objs_count in range(objs.shape[0]):
    ob_color=[]
    if pred_clas_selected[objs_count] == 1:
        ob_color = [0, 0, 255]
    if pred_clas_selected[objs_count] == 2:
        ob_color = [255, 0, 0]
    if pred_clas_selected[objs_count] == 3:
        ob_color = [255, 0, 255]
    # ob_color = [255, 0, 255]
    start = (objs[objs_count][0][0], objs[objs_count][0][1])
    end = (objs[objs_count][1][0], objs[objs_count][1][1])
    cv2.line(image_data, start, end, ob_color, thickness, line_type)
    start = (objs[objs_count][1][0], objs[objs_count][1][1])
    end = (objs[objs_count][2][0], objs[objs_count][2][1])
    cv2.line(image_data, start, end, ob_color, thickness, line_type)
    start = (objs[objs_count][2][0], objs[objs_count][2][1])
    end = (objs[objs_count][3][0], objs[objs_count][3][1])
    cv2.line(image_data, start, end, ob_color, thickness, line_type)
    start = (objs[objs_count][3][0], objs[objs_count][3][1])
    end = (objs[objs_count][0][0], objs[objs_count][0][1])
    cv2.line(image_data, start, end, ob_color, thickness, line_type)
    start = (objs[objs_count][4][0], objs[objs_count][4][1])
    end = (objs[objs_count][5][0], objs[objs_count][5][1])
    cv2.line(image_data, start, end, ob_color, thickness, line_type)
    start = (objs[objs_count][6][0], objs[objs_count][6][1])
    end = (objs[objs_count][5][0], objs[objs_count][5][1])
    cv2.line(image_data, start, end, ob_color, thickness, line_type)
    start = (objs[objs_count][6][0], objs[objs_count][6][1])
    end = (objs[objs_count][7][0], objs[objs_count][7][1])
    cv2.line(image_data, start, end, ob_color, thickness, line_type)
    start = (objs[objs_count][4][0], objs[objs_count][4][1])
    end = (objs[objs_count][7][0], objs[objs_count][7][1])
    cv2.line(image_data, start, end, ob_color, thickness, line_type)
    start = (objs[objs_count][4][0], objs[objs_count][4][1])
    end = (objs[objs_count][0][0], objs[objs_count][0][1])
    cv2.line(image_data, start, end, ob_color, thickness, line_type)
    start = (objs[objs_count][1][0], objs[objs_count][1][1])
    end = (objs[objs_count][5][0], objs[objs_count][5][1])
    cv2.line(image_data, start, end, ob_color, thickness, line_type)
    start = (objs[objs_count][6][0], objs[objs_count][6][1])
    end = (objs[objs_count][2][0], objs[objs_count][2][1])
    cv2.line(image_data, start, end, ob_color, thickness, line_type)
    start = (objs[objs_count][3][0], objs[objs_count][3][1])
    end = (objs[objs_count][7][0], objs[objs_count][7][1])
    cv2.line(image_data, start, end, ob_color, thickness, line_type)
cv2.imshow("image_data", image_data)
cv2.waitKey(0)
# time.sleep(50000)
