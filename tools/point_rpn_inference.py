import _init_path
import open3d as o3d
import cv2
import torch
import torch.optim as optim
import torch.optim.lr_scheduler as lr_sched
import torch.nn as nn
from torch.utils.data import DataLoader
from tensorboardX import SummaryWriter
import torch.nn.functional as F
import os
import argparse
import logging
from functools import partial
from collections import OrderedDict
import numpy as np
import lib.utils.kitti_utils as kitti_utils
from lib.net.point_rcnn import PointRCNN
import lib.net.train_functions as train_functions
from lib.datasets.kitti_rcnn_dataset import KittiRCNNDataset
from lib.config import cfg, cfg_from_file, save_config_to_file
import tools.train_utils.train_utils as train_utils
from tools.train_utils.fastai_optim import OptimWrapper
from tools.train_utils import learning_schedules_fastai as lsf

parser = argparse.ArgumentParser(description="arg parser")
parser.add_argument('--cfg_file', type=str, default='cfgs/default.yaml', help='specify the config for training')
parser.add_argument("--train_mode", type=str, default='rpn', required=False, help="specify the training mode")
parser.add_argument("--batch_size", type=int, default=1, required=False, help="batch size for training")
# 当设置为required=True时,则在运行程序时,必须输入此参数
parser.add_argument("--epochs", type=int, default=200, required=False, help="Number of epochs to train for")

parser.add_argument('--workers', type=int, default=8, help='number of workers for dataloader')
parser.add_argument("--ckpt_save_interval", type=int, default=5, help="number of training epochs")
parser.add_argument('--output_dir', type=str, default=None, help='specify an output directory if needed')
parser.add_argument('--mgpus', action='store_true', default=False, help='whether to use multiple gpu')

parser.add_argument("--ckpt", type=str, default=None, help="continue training from this checkpoint")
parser.add_argument("--rpn_ckpt", type=str, default=None, help="specify the well-trained rpn checkpoint")

parser.add_argument("--gt_database", type=str, default='gt_database/train_gt_database_3level_Car.pkl',
                    help='generated gt database for augmentation')
parser.add_argument("--rcnn_training_roi_dir", type=str, default=None,
                    help='specify the saved rois for rcnn training when using rcnn_offline mode')
parser.add_argument("--rcnn_training_feature_dir", type=str, default=None,
                    help='specify the saved features for rcnn training when using rcnn_offline mode')

parser.add_argument('--train_with_eval', action='store_true', default=False, help='whether to train with evaluation')
parser.add_argument("--rcnn_eval_roi_dir", type=str, default=None,
                    help='specify the saved rois for rcnn evaluation when using rcnn_offline mode')
parser.add_argument("--rcnn_eval_feature_dir", type=str, default=None,
                    help='specify the saved features for rcnn evaluation when using rcnn_offline mode')
args = parser.parse_args()


def create_logger(log_file):
    log_format = '%(asctime)s  %(levelname)5s  %(message)s'
    logging.basicConfig(level=logging.DEBUG, format=log_format, filename=log_file)
    console = logging.StreamHandler()
    console.setLevel(logging.DEBUG)
    console.setFormatter(logging.Formatter(log_format))
    logging.getLogger(__name__).addHandler(console)
    return logging.getLogger(__name__)


def create_dataloader(logger):
    DATA_PATH = "/home/yokybank/anaconda3/envs/PointRCNNTest/data/"
    # create dataloader
    # --rcnn_training_roi_dir, default=None
    # --rcnn_training_feature_dir, type=str, default=None
    # CLASSES: Car
    # cfg.RPN.NUM_POINTS: 16384
    # cfg.RCNN.NUM_POINTS: 512
    # TRAIN:
    # SPLIT: train
    # TEST:
    # SPLIT: val
    train_set = KittiRCNNDataset(root_dir=DATA_PATH, npoints=cfg.RPN.NUM_POINTS, split=cfg.TRAIN.SPLIT, mode='TEST',
                                 logger=logger,
                                 classes=cfg.CLASSES,
                                 rcnn_training_roi_dir=args.rcnn_training_roi_dir,
                                 rcnn_training_feature_dir=args.rcnn_training_feature_dir,
                                 gt_database_dir=args.gt_database)
    # =cfg.CLASSES:Car NUM_POINTS: 16384  cfg.TRAIN.SPLIT: train
    train_loader = DataLoader(train_set, batch_size=args.batch_size, pin_memory=True,
                              num_workers=args.workers, shuffle=True, collate_fn=train_set.collate_batch,
                              drop_last=True)

    if args.train_with_eval:  # default=False
        test_set = KittiRCNNDataset(root_dir=DATA_PATH, npoints=cfg.RPN.NUM_POINTS, split=cfg.TRAIN.VAL_SPLIT,
                                    mode='EVAL',
                                    logger=logger,
                                    classes=cfg.CLASSES,
                                    rcnn_eval_roi_dir=args.rcnn_eval_roi_dir,
                                    rcnn_eval_feature_dir=args.rcnn_eval_feature_dir)
        test_loader = DataLoader(test_set, batch_size=1, shuffle=True, pin_memory=True,
                                 num_workers=args.workers, collate_fn=test_set.collate_batch)
    else:
        test_loader = None
    return train_loader, test_loader


def create_optimizer(model):
    # 配置文件中是cfg.TRAIN.OPTIMIZER = 'adam_onecycle'
    if cfg.TRAIN.OPTIMIZER == 'adam':
        optimizer = optim.Adam(model.parameters(), lr=cfg.TRAIN.LR, weight_decay=cfg.TRAIN.WEIGHT_DECAY)
    elif cfg.TRAIN.OPTIMIZER == 'sgd':
        optimizer = optim.SGD(model.parameters(), lr=cfg.TRAIN.LR, weight_decay=cfg.TRAIN.WEIGHT_DECAY,
                              momentum=cfg.TRAIN.MOMENTUM)
    elif cfg.TRAIN.OPTIMIZER == 'adam_onecycle':
        def children(m: nn.Module):
            return list(m.children())

        def num_children(m: nn.Module) -> int:
            return len(children(m))

        # map:第一个参数flatten_model函数对序列m.children()中的每一个元素调用flatten_model函数对序列进行运算，返回包含每次 function 函数返回值的新列表
        # 最终是将model作为输入,也即model=m
        # 采用递归的方式得到每层网络,因为num_children(m)迟早为0,将返回[m]
        flatten_model = lambda m: sum(map(flatten_model, m.children()), []) if num_children(m) else [m]
        get_layer_groups = lambda m: [nn.Sequential(*flatten_model(m))]

        # partial函数的功能就是：把一个函数的某些参数给固定住，返回一个新的函数。需要注意的是，我们上面是固定了
        # multiply的关键字参数y = 2，如果直接使用：double = partial(multiply, 2)则2是赋给了multiply最左边的参数x

        # WEIGHT_DECAY: 0.001  # L2 regularization
        optimizer_func = partial(optim.Adam, betas=(0.9, 0.99))
        optimizer = OptimWrapper.create(
            optimizer_func, 3e-3, get_layer_groups(model), wd=cfg.TRAIN.WEIGHT_DECAY, true_wd=True, bn_wd=True
        )

        # fix rpn: do this since we use costomized optimizer.step
        if cfg.RPN.ENABLED and cfg.RPN.FIXED:
            for param in model.rpn.parameters():
                param.requires_grad = False
    else:
        raise NotImplementedError

    return optimizer


def create_scheduler(optimizer, total_steps, last_epoch):
    def lr_lbmd(cur_epoch):
        cur_decay = 1
        for decay_step in cfg.TRAIN.DECAY_STEP_LIST:
            if cur_epoch >= decay_step:
                cur_decay = cur_decay * cfg.TRAIN.LR_DECAY
        return max(cur_decay, cfg.TRAIN.LR_CLIP / cfg.TRAIN.LR)

    def bnm_lmbd(cur_epoch):
        cur_decay = 1
        for decay_step in cfg.TRAIN.BN_DECAY_STEP_LIST:
            if cur_epoch >= decay_step:
                cur_decay = cur_decay * cfg.TRAIN.BN_DECAY
        return max(cfg.TRAIN.BN_MOMENTUM * cur_decay, cfg.TRAIN.BNM_CLIP)

    if cfg.TRAIN.OPTIMIZER == 'adam_onecycle':
        lr_scheduler = lsf.OneCycle(
            optimizer, total_steps, cfg.TRAIN.LR, list(cfg.TRAIN.MOMS), cfg.TRAIN.DIV_FACTOR, cfg.TRAIN.PCT_START
        )
    else:
        lr_scheduler = lr_sched.LambdaLR(optimizer, lr_lbmd, last_epoch=last_epoch)

    bnm_scheduler = train_utils.BNMomentumScheduler(model, bnm_lmbd, last_epoch=last_epoch)
    return lr_scheduler, bnm_scheduler


def get_retangle_line_set(points, color, line_count):
    points = points.tolist()
    lines = [[0 + line_count * 8, 1 + line_count * 8],
             [1 + line_count * 8, 2 + line_count * 8],
             [2 + line_count * 8, 3 + line_count * 8],
             [3 + line_count * 8, 0 + line_count * 8],
             [4 + line_count * 8, 5 + line_count * 8],
             [5 + line_count * 8, 6 + line_count * 8],
             [6 + line_count * 8, 7 + line_count * 8],
             [7 + line_count * 8, 4 + line_count * 8],
             [0 + line_count * 8, 4 + line_count * 8],
             [1 + line_count * 8, 5 + line_count * 8],
             [2 + line_count * 8, 6 + line_count * 8],
             [3 + line_count * 8, 7 + line_count * 8],
             ]
    colors = [np.asarray(color).tolist() for i in range(12)]

    return points, colors, lines


if __name__ == "__main__":
    if args.cfg_file is not None:
        cfg_from_file(args.cfg_file)
        # 将配置文件的值赋值给cfg
    cfg.TAG = os.path.splitext(os.path.basename(args.cfg_file))[0]
    # os.path.splitext按最后一个小数点分割,返回元组.
    # os.path.basename(args.cfg_file)返回剔除路径后的文件名

    if args.train_mode == 'rpn':
        cfg.RPN.ENABLED = True
        cfg.RCNN.ENABLED = False
        root_result_dir = os.path.join('../', 'output', 'rpn', cfg.TAG)
    elif args.train_mode == 'rcnn':
        cfg.RCNN.ENABLED = True
        cfg.RPN.ENABLED = cfg.RPN.FIXED = True
        root_result_dir = os.path.join('../', 'output', 'rcnn', cfg.TAG)
    elif args.train_mode == 'rcnn_offline':
        cfg.RCNN.ENABLED = True
        cfg.RPN.ENABLED = False
        root_result_dir = os.path.join('../', 'output', 'rcnn', cfg.TAG)
    else:
        raise NotImplementedError

    if args.output_dir is not None:
        root_result_dir = args.output_dir
    os.makedirs(root_result_dir, exist_ok=True)
    # 如果exist_ok是False（默认），当目标目录（即要创建的目录）已经存在，会抛出一个OSError,为True时则不会抛出OSError
    log_file = os.path.join(root_result_dir, 'log_train.txt')
    logger = create_logger(log_file)
    logger.info('**********************Start logging**********************')

    # log to file
    # os.environ.keys()返回所有环境变量的key的集合,os.environ以字典的方式保留
    gpu_list = os.environ['CUDA_VISIBLE_DEVICES'] if 'CUDA_VISIBLE_DEVICES' in os.environ.keys() else 'ALL'
    # print("gpu_list:{}".format(gpu_list))
    logger.info('CUDA_VISIBLE_DEVICES=%s' % gpu_list)

    # vars函数返回对象object的属性和属性值的字典对象
    for key, val in vars(args).items():
        logger.info("{:16} {}".format(key, val))

    save_config_to_file(cfg, logger=logger)

    # copy important files to backup
    backup_dir = os.path.join(root_result_dir, 'backup_files')
    os.makedirs(backup_dir, exist_ok=True)
    os.system('cp *.py %s/' % backup_dir)
    os.system('cp ../lib/net/*.py %s/' % backup_dir)
    os.system('cp ../lib/datasets/kitti_rcnn_dataset.py %s/' % backup_dir)

    # tensorboard log
    tb_log = SummaryWriter(log_dir=os.path.join(root_result_dir, 'tensorboard'))

    # create dataloader & network & optimizer
    train_loader, test_loader = create_dataloader(logger)
    model = PointRCNN(num_classes=train_loader.dataset.num_class, use_xyz=True, mode='TEST')
    model = nn.DataParallel(model)
    model.cuda()
    pure_model = model.module if isinstance(model, torch.nn.DataParallel) else model
    total_keys = pure_model.state_dict().keys().__len__()
    file_path = "../output/rpn/default/ckpt/checkpoint_epoch_200.pth"
    train_utils.load_part_ckpt(pure_model, filename=file_path, logger=logger, total_keys=total_keys)
    anchor_size = np.asarray([1.52563191462, 1.62856739989, 3.88311640418])
    dataset = train_loader.dataset
    for cur_it, batch in enumerate(train_loader):
        if cfg.RPN.ENABLED:
            pts_rect, pts_features, pts_input = batch['pts_rect'], batch['pts_features'], batch['pts_input']
            gt_boxes3d = batch['gt_boxes3d']
            sample_id = batch['sample_id']
            # print("batch['sample_id']:{}".format(batch['sample_id']))

            # rpn_cls_label, rpn_reg_label = batch['rpn_cls_label'], batch['rpn_reg_label']
            # rpn_cls_label = torch.from_numpy(rpn_cls_label).cuda(non_blocking=True).long()
            # rpn_reg_label = torch.from_numpy(rpn_reg_label).cuda(non_blocking=True).float()
            # print("gt_boxes3d:{}".format(gt_boxes3d))
            inputs = torch.from_numpy(pts_input).cuda(non_blocking=True).float()
            gt_boxes3d = torch.from_numpy(gt_boxes3d).cuda(non_blocking=True).float()
            input_data = {'pts_input': inputs, 'gt_boxes3d': gt_boxes3d}
        ret_dict = model(input_data)
        pts_input = pts_input[0, :, :]
        # print(pts_input.shape)

        rpn_reg = ret_dict['rpn_reg']
        rpn_cls = ret_dict['rpn_cls']

        rpn_cls = F.softmax(rpn_cls.view(-1, 4), dim=1).cpu()
        probility, pred = torch.max(rpn_cls, dim=1)
        pred = pred.numpy()
        probability = probility.numpy()
        # print("pred:{}".format(pred))
        # print("probability:{}".format(probability))
        # print("rpn_cls:{}".format(pred.numpy()))
        # print()

        # rpn_reg = rpn_reg.view(rpn_reg.shape[1], 76).cpu().numpy()
        # 0为1,则为背景点，1为1则为人，2为1则为骑行者，3为1则为车
        fg_person_pts_flag = (pred == 1)
        fg_cyclist_pts_flag = (pred == 2)
        fg_car_pts_flag = (pred == 3)
        bg_pts_flag = (pred == 0)

        fg_person_pts = pts_input[fg_person_pts_flag]
        fg_cyclist_pts = pts_input[fg_cyclist_pts_flag]
        fg_car_pts = pts_input[fg_car_pts_flag]
        bg_pts = pts_input[bg_pts_flag]
        angle_per_class = (2 * np.pi) / 12
        # print("rpn_reg:{}".format(type(rpn_reg)))
        rpn_reg = (rpn_reg.view(-1, 117)).cpu().numpy()
        fg_person_rpn_reg = rpn_reg[fg_person_pts_flag]
        fg_cyclist_rpn_reg = rpn_reg[fg_cyclist_pts_flag]
        fg_car_rpn_reg = rpn_reg[fg_car_pts_flag]
        proposals = []
        data_to_draw = []

        all_points = []
        all_colors = []
        all_lines = []

        for i in range(fg_person_rpn_reg.shape[0]):
            # print(fg_person_rpn_reg[i,0:12])
            x_bin = np.argmax(fg_person_rpn_reg[i, 0:12])
            x_shift = fg_person_rpn_reg[i, 24:36][x_bin] + 0.25 + x_bin * 0.5
            box_x = x_shift - 3 + fg_person_pts[i, 0]

            z_bin = np.argmax(fg_person_rpn_reg[i, 12:24])
            # print(fg_person_rpn_reg[i,36:48])
            z_shift = fg_person_rpn_reg[i, 36:48][z_bin] + 0.25 + z_bin * 0.5
            box_z = z_shift - 3 + fg_person_pts[i, 2]

            box_y = fg_person_rpn_reg[i, 48] + fg_person_pts[i, 1]

            ry_bin = np.argmax(fg_person_rpn_reg[i, 49:61])
            shift_angle = fg_person_rpn_reg[i, 61:73][ry_bin] * (angle_per_class / 2) + (
                    angle_per_class / 2) + ry_bin * angle_per_class
            heading_angle = (shift_angle - (angle_per_class / 2)) % (2 * np.pi)
            if heading_angle > np.pi:
                heading_angle = heading_angle - 2 * np.pi

            height_bin = np.argmax(fg_person_rpn_reg[i, 73:79])
            height_shift = fg_person_rpn_reg[i, 95:101][height_bin] * 0.5 + 0.25 + height_bin * 0.5
            width_bin = np.argmax(fg_person_rpn_reg[i, 79:85])
            width_shift = fg_person_rpn_reg[i, 101:107][width_bin] * 0.5 + 0.25 + width_bin * 0.5
            length_bin = np.argmax(fg_person_rpn_reg[i, 85:95])
            length_shift = fg_person_rpn_reg[i, 107:117][length_bin] * 1.0 + 0.5 + length_bin * 1
            size = np.asarray([height_shift, width_shift, length_shift])

            box_y = box_y + size[0] / 2
            boxes3d = np.asarray([[box_x, box_y, box_z, size[0], size[1], size[2], heading_angle]])
            gt_corners = kitti_utils.boxes3d_to_corners3d(boxes3d, rotate=True)
            temp_points, temp_colors, temp_lines = get_retangle_line_set(gt_corners[0, :, :], [0, 0, 0], i)
            all_points.extend(temp_points)
            all_colors.extend(temp_colors)
            all_lines.extend(temp_lines)

        for i in range(fg_cyclist_rpn_reg.shape[0]):
            x_bin = np.argmax(fg_cyclist_rpn_reg[i, 0:12])
            x_shift = fg_cyclist_rpn_reg[i, 24:36][x_bin] + 0.25 + x_bin * 0.5
            box_x = x_shift - 3 + fg_cyclist_pts[i, 0]

            z_bin = np.argmax(fg_cyclist_rpn_reg[i, 12:24])
            # print(fg_cyclist_rpn_reg[i,36:48])
            z_shift = fg_cyclist_rpn_reg[i, 36:48][z_bin] + 0.25 + z_bin * 0.5
            box_z = z_shift - 3 + fg_cyclist_pts[i, 2]

            box_y = fg_cyclist_rpn_reg[i, 48] + fg_cyclist_pts[i, 1]

            ry_bin = np.argmax(fg_cyclist_rpn_reg[i, 49:61])
            shift_angle = fg_cyclist_rpn_reg[i, 61:73][ry_bin] * (angle_per_class / 2) + (
                    angle_per_class / 2) + ry_bin * angle_per_class
            heading_angle = (shift_angle - (angle_per_class / 2)) % (2 * np.pi)
            if heading_angle > np.pi:
                heading_angle = heading_angle - 2 * np.pi

            height_bin = np.argmax(fg_cyclist_rpn_reg[i, 73:79])
            height_shift = fg_cyclist_rpn_reg[i, 95:101][height_bin] * 0.5 + 0.25 + height_bin * 0.5
            width_bin = np.argmax(fg_cyclist_rpn_reg[i, 79:85])
            width_shift = fg_cyclist_rpn_reg[i, 101:107][width_bin] * 0.5 + 0.25 + width_bin * 0.5
            length_bin = np.argmax(fg_cyclist_rpn_reg[i, 85:95])
            length_shift = fg_cyclist_rpn_reg[i, 107:117][length_bin] * 1 + 0.5 + length_bin * 1
            size = np.asarray([height_shift, width_shift, length_shift])

            box_y = box_y + size[0] / 2
            boxes3d = np.asarray([[box_x, box_y, box_z, size[0], size[1], size[2], heading_angle]])
            gt_corners = kitti_utils.boxes3d_to_corners3d(boxes3d, rotate=True)
            temp_points, temp_colors, temp_lines = get_retangle_line_set(gt_corners[0, :, :], [0, 0, 0],
                                                                         i + fg_person_rpn_reg.shape[0])
            all_points.extend(temp_points)
            all_colors.extend(temp_colors)
            all_lines.extend(temp_lines)
            # break
        # num_count = 0
        for i in range(fg_car_rpn_reg.shape[0]):
            x_bin = np.argmax(fg_car_rpn_reg[i, 0:12])
            x_shift = fg_car_rpn_reg[i, 24:36][x_bin] + 0.25 + x_bin * 0.5
            box_x = x_shift - 3 + fg_car_pts[i, 0]

            z_bin = np.argmax(fg_car_rpn_reg[i, 12:24])
            # print(fg_car_rpn_reg[i,36:48])
            z_shift = fg_car_rpn_reg[i, 36:48][z_bin] + 0.25 + z_bin * 0.5
            box_z = z_shift - 3 + fg_car_pts[i, 2]

            box_y = fg_car_rpn_reg[i, 48] + fg_car_pts[i, 1]

            ry_bin = np.argmax(fg_car_rpn_reg[i, 49:61])
            shift_angle = fg_car_rpn_reg[i, 61:73][ry_bin] * (angle_per_class / 2) + (
                    angle_per_class / 2) + ry_bin * angle_per_class
            heading_angle = (shift_angle - (angle_per_class / 2)) % (2 * np.pi)
            if heading_angle > np.pi:
                heading_angle = heading_angle - 2 * np.pi

            height_bin = np.argmax(fg_car_rpn_reg[i, 73:79])
            height_shift = fg_car_rpn_reg[i, 95:101][height_bin] + 0.25 + height_bin * 0.5
            width_bin = np.argmax(fg_car_rpn_reg[i, 79:85])
            width_shift = fg_car_rpn_reg[i, 101:107][width_bin] + 0.25 + width_bin * 0.5
            length_bin = np.argmax(fg_car_rpn_reg[i, 85:95])
            length_shift = fg_car_rpn_reg[i, 107:117][length_bin] + 0.5 + length_bin * 1
            size = np.asarray([height_shift, width_shift, length_shift])

            box_y = box_y + size[0] / 2
            boxes3d = np.asarray([[box_x, box_y, box_z, size[0], size[1], size[2], heading_angle]])
            gt_corners = kitti_utils.boxes3d_to_corners3d(boxes3d, rotate=True)
            temp_points, temp_colors, temp_lines = get_retangle_line_set(gt_corners[0, :, :], [0, 0, 0],
                                                                         i + fg_person_rpn_reg.shape[0] +
                                                                         fg_cyclist_rpn_reg.shape[0])
            all_points.extend(temp_points)
            all_colors.extend(temp_colors)
            all_lines.extend(temp_lines)
            #
            # if num_count == 0:
            #     all_points.extend(temp_points)
            #
            #     all_colors.extend(temp_colors)
            #     all_lines.extend(temp_lines)
            #     num_count += 1
            #     boxes3d = np.asarray([[box_x, box_y, box_z, size[0] - 1, size[1] - 1, size[2] - 1, heading_angle]])
            #     gt_corners = kitti_utils.boxes3d_to_corners3d(boxes3d, rotate=True)
            #     temp_points, temp_colors, temp_lines = get_retangle_line_set(gt_corners[0, :, :], [0, 0, 0],
            #                                                                  i + 1 + fg_person_rpn_reg.shape[0] +
            #                                                                  fg_cyclist_rpn_reg.shape[0])
            #     all_colors.extend(temp_colors)
            #     all_points.extend(temp_points)
            #     all_lines.extend(temp_lines)

        cur_sample_id = sample_id[0]
        image_data_path = dataset.get_image_path(cur_sample_id)
        print("image_data_path:{}".format(image_data_path))
        with open("./image_path.txt", "w") as f:
            f.write(image_data_path)  # 自带文件关闭功能，不需要再写f.close()
            f.close()
        # cv2.imshow(str(cur_sample_id), image_data)
        # cv2.waitKey(0)  # ms
        # print("cur_sample_id:{}".format(cur_sample_id))
        line_set = o3d.geometry.LineSet(
            points=o3d.utility.Vector3dVector(all_points),
            lines=o3d.utility.Vector2iVector(all_lines))
        line_set.colors = o3d.utility.Vector3dVector(all_colors)
        data_to_draw.extend([line_set])

        # data_to_draw=[]
        FOR1 = o3d.geometry.TriangleMesh.create_coordinate_frame(size=3, origin=[0, 0, 0])
        pcd = o3d.geometry.PointCloud()
        # print(fp_pts.shape)
        pcd.points = o3d.utility.Vector3dVector(fg_person_pts[:, 0:3])
        pcd.paint_uniform_color([1, 0, 0])

        pcd1 = o3d.geometry.PointCloud()
        # print(fp_pts.shape)
        pcd1.points = o3d.utility.Vector3dVector(fg_cyclist_pts[:, 0:3])
        pcd1.paint_uniform_color([0, 0, 1])

        pcd3 = o3d.geometry.PointCloud()
        # print(fp_pts.shape)
        pcd3.points = o3d.utility.Vector3dVector(fg_car_pts[:, 0:3])
        pcd3.paint_uniform_color([1, 0, 1])

        pcd2 = o3d.geometry.PointCloud()
        pcd2.points = o3d.utility.Vector3dVector(bg_pts[:, 0:3])
        pcd2.paint_uniform_color([0.3, 0.8, 0.3])
        data_to_draw.extend([FOR1, pcd, pcd1, pcd2, pcd3])
        o3d.visualization.draw_geometries(data_to_draw)
