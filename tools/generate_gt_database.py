import _init_path
import os
import numpy as np
import pickle
import torch

import lib.utils.roipool3d.roipool3d_utils as roipool3d_utils
from lib.datasets.kitti_dataset import KittiDataset
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--save_dir', type=str, default='./gt_database')
parser.add_argument('--class_name', type=str, default='All')
parser.add_argument('--split', type=str, default='train')
args = parser.parse_args()


class GTDatabaseGenerator(KittiDataset):
    def __init__(self, root_dir, split='train', classes=args.class_name):
        super().__init__(root_dir, split=split)
        self.gt_database = None
        if classes == 'Car':
            self.classes = ('Background', 'Car')
        elif classes == 'People':
            self.classes = ('Background', 'Pedestrian', 'Cyclist')
        elif classes == 'Pedestrian':
            self.classes = ('Background', 'Pedestrian')
        elif classes == 'Cyclist':
            self.classes = ('Background', 'Cyclist')
        elif classes == 'All':
            self.classes = ('Background', 'Car', 'Pedestrian', 'Cyclist','Van','Person_sitting')
        else:
            assert False, "Invalid classes: %s" % classes
        print(self.classes)

    def __len__(self):
        raise NotImplementedError

    def __getitem__(self, item):
        raise NotImplementedError
    def get_valid_flag(self,pts_rect, pts_img, pts_rect_depth, img_shape):
        """
        Valid point should be in the image (and in the PC_AREA_SCOPE)
        :param pts_rect:
        :param pts_img:
        :param pts_rect_depth:
        :param img_shape:
        :return:
        """
        PC_REDUCE_BY_RANGE=True
        PC_AREA_SCOPE=[[-40, 40], [-1, 3], [0, 70.4]]  # x, y, z scope in rect camera coords
        # 对每个元素进行 and 运算，返回 bool 值组成的数组
        val_flag_1 = np.logical_and(pts_img[:, 0] >= 0, pts_img[:, 0] < img_shape[1])
        val_flag_2 = np.logical_and(pts_img[:, 1] >= 0, pts_img[:, 1] < img_shape[0])
        val_flag_merge = np.logical_and(val_flag_1, val_flag_2)
        pts_valid_flag = np.logical_and(val_flag_merge, pts_rect_depth >= 0)

        if PC_REDUCE_BY_RANGE:
            x_range, y_range, z_range = PC_AREA_SCOPE
            pts_x, pts_y, pts_z = pts_rect[:, 0], pts_rect[:, 1], pts_rect[:, 2]
            range_flag = (pts_x >= x_range[0]) & (pts_x <= x_range[1]) \
                         & (pts_y >= y_range[0]) & (pts_y <= y_range[1]) \
                         & (pts_z >= z_range[0]) & (pts_z <= z_range[1])
            pts_valid_flag = pts_valid_flag & range_flag
        return pts_valid_flag

    def filtrate_objects(self, obj_list):
        #过滤冗杂目标
        valid_obj_list = []
        for obj in obj_list:
            if obj.cls_type not in self.classes:
                continue
            if obj.level_str not in ['Easy', 'Moderate', 'Hard']:
                continue
            valid_obj_list.append(obj)

        return valid_obj_list

    def generate_gt_database(self):
        gt_database = []
        for idx, sample_id in enumerate(self.image_idx_list):#image_idx_list所有读到的索引
            sample_id = int(sample_id)
            print('process gt sample (id=%06d)' % sample_id)

            pts_lidar = self.get_lidar(sample_id)#将kitti的点云数据返回为(-1,4)的np.array数组
            calib = self.get_calib(sample_id)#返回对应的标定文件


            pts_rect = calib.lidar_to_rect(pts_lidar[:, 0:3])
            img_shape = self.get_image_shape(sample_id % 10000)
            pts_img, pts_rect_depth = calib.rect_to_img(pts_rect)
            pts_valid_flag = self.get_valid_flag(pts_rect, pts_img, pts_rect_depth, img_shape)

            cur_img = self.get_image(sample_id % 10000)
            corresponding_BGR = []
            for i in range(len(pts_img[:, 0])):
                if pts_valid_flag[i]:
                    temp_color = cur_img[int(pts_img[i, 1]), int(pts_img[i, 0])] / 255.0
                else:
                    temp_color = [0,0,0]
                corresponding_BGR.append([temp_color[2], temp_color[1], temp_color[0]])
            corresponding_RGB = np.asarray(corresponding_BGR)

            pts_intensity = pts_lidar[:, 3]
            obj_list = self.filtrate_objects(self.get_label(sample_id))
            #self.get_label(sample_id)得到的是第sample_id帧所有目标的label值
            gt_boxes3d = np.zeros((obj_list.__len__(), 7), dtype=np.float32)
            #第sample_id帧的所有label对应的数据存储在gt_boxes3d中
            for k, obj in enumerate(obj_list):
                gt_boxes3d[k, 0:3], gt_boxes3d[k, 3], gt_boxes3d[k, 4], gt_boxes3d[k, 5], gt_boxes3d[k, 6] \
                    = obj.pos, obj.h, obj.w, obj.l, obj.ry

            if gt_boxes3d.__len__() == 0:
                print('No gt object')
                continue
            #boxes_pts_mask_list中每一行的数量是一帧点云的数量,行数等于一个sample_id的label包含的gt的数量
            #boxes_pts_mask_list中属于gt_box内的点云会置1,不属于会置0,boxes_pts_mask_list大小为M,M是GT_box的个数,每个M包含N个点云属于gt_box内的点云会置1,不属于会置0,
            boxes_pts_mask_list = roipool3d_utils.pts_in_boxes3d_cpu(torch.from_numpy(pts_rect), torch.from_numpy(gt_boxes3d))

            for k in range(boxes_pts_mask_list.__len__()):
                pt_mask_flag = (boxes_pts_mask_list[k].numpy() == 1)
                #boxes_pts_mask_list[k]中等于1的变为True,不等于1的变为False,矩阵大小不变,矩阵元素为bool型
                #boxes_pts_mask_list[k].numpy() == 1的结果为一个数值list,而非一个矩阵

                cur_pts = pts_rect[pt_mask_flag].astype(np.float32)#将pt_mask_flag为true的部分的点找出来,组成一个新的矩阵赋值给cur_pts,所以cur_pts为在gt_box内的点
                cur_pts_intensity = pts_intensity[pt_mask_flag].astype(np.float32)
                cur_pts_rgb=corresponding_RGB[pt_mask_flag].astype(np.float32)
                sample_dict = {'sample_id': sample_id,
                               'cls_type': obj_list[k].cls_type,
                               'gt_box3d': gt_boxes3d[k],
                               'points': cur_pts,
                               'intensity': cur_pts_intensity,
                               'obj': obj_list[k],
                               'rgb':cur_pts_rgb}
                #cur_pts是点在gt_box内的点的集合
                gt_database.append(sample_dict)

        save_file_name = os.path.join(args.save_dir, '%s_gt_database_3level_%s.pkl' % (args.split, self.classes[-1]))
        with open(save_file_name, 'wb') as f:
            pickle.dump(gt_database, f)
        # pickle.dump序列化对象，将对象obj保存到文件file中去。参数protocol是序列化模式，
        # 默认是0（ASCII协议，表示以文本的形式进行序列化），
        # protocol的值还可以是1和2（1和2表示以二进制的形式进行序列化。其中，1是老式的二进制协议；2是新二进制协议）。
        self.gt_database = gt_database
        print('Save refine training sample info file to %s' % save_file_name)


if __name__ == '__main__':
    dataset = GTDatabaseGenerator(root_dir='../data/', split=args.split)
    os.makedirs(args.save_dir, exist_ok=True)
    dataset.generate_gt_database()

    # gt_database = pickle.load(open('gt_database/train_gt_database.pkl', 'rb'))
    # print(gt_database.__len__())
    # import pdb
    # pdb.set_trace()
